# Project 4: Brevet time calculator with Ajax
By Rich Hastings (ghasting@uoregon.edu)

Reimplement the RUSA ACP controle time calculator with flask and ajax.

Credits to Michal Young for the initial version of this code.

## ACP controle times

That's "controle" with an 'e', because it's French, although "control" is also accepted. Controls are points where a rider must obtain proof of passage, and control[e] times are the minimum and maximum times by which the rider must arrive at the location.   

The algorithm for calculating controle times is described here (https://rusa.org/pages/acp-brevet-control-times-calculator). Additional background information is given here (https://rusa.org/pages/rulesForRiders). 

This is does not use the French algorithm. I assumed that it was not to be used as instructions were provided in english.
This Program will limit times of control distance to the total distance of the Brevet. I
The endtime for a 200 km Brevet has been set to 13 hours and 30 as per the stated rules.

Includes a basic test suit for the acp times functions
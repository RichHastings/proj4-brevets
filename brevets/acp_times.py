"""
Open and close time calculations
for ACP-sanctioned brevets
following rules described at https://rusa.org/octime_alg.html
and https://rusa.org/pages/rulesForRiders
"""
import arrow
from math import floor

#  Note for CIS 322 Fall 2016:
#  You MUST provide the following two functions
#  with these signatures, so that I can write
#  automated tests for grading.  You must keep
#  these signatures even if you don't use all the
#  same arguments.  Arguments are explained in the
#  javadoc comments.
#


def open_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
       brevet_dist_km: number, the nominal distance of the brevet
           in kilometers, which must be one of 200, 300, 400, 600,
           or 1000 (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control open time.
       This will be in the same time zone as the brevet start time.
    """
    if brevet_dist_km > control_dist_km:
        total = control_dist_km
    else:
        total = brevet_dist_km
    values=[[200, 34], [200, 32], [200, 30], [400, 28], [300, 26]]
    retval = 0
    #total = brevet_dist_km
    endtime = arrow.get(brevet_start_time)
    control=control_dist_km
    i=0
    if total == 0:
        return endtime.isoformat()
    while total > 0:
        if total < values[i][0]:
            add = total/values[i][1]
            retval +=add
            total -= values[i][0]
        else:
            add = values[i][0]/values[i][1]
            retval += add
            total -= values[i][0]
            i += 1
    hour = floor(retval)
    minute = retval - hour
    minute = minute * 60
    minute = round(minute)

    #endtime=endtime.shift(minutes=+minute)
    endtime=endtime.shift(hours=+hour, minutes=+minute)

    return endtime.isoformat()


def close_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
          brevet_dist_km: number, the nominal distance of the brevet
          in kilometers, which must be one of 200, 300, 400, 600, or 1000
          (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indic ating the control close time.
       This will be in the same time zone as the brevet start time."""
    if brevet_dist_km > control_dist_km:
        total = control_dist_km
    else:
        total = brevet_dist_km
    #total=control_dist_km
    values = [[600, 15], [400, 11.428], [300, 13.333]]
    retval = 0
    endtime = arrow.get(brevet_start_time)
    control = control_dist_km
    i = 0

    if total==200 and brevet_dist_km==200:
        endtime=endtime.shift(hours=+13, minutes=+30)
        return endtime.isoformat()

    if total == 0:
        endtime= endtime.shift(hours=+1)
        return endtime.isoformat()
    while total > 0:
        if total < values[i][0]:
            add = total / values[i][1]
            retval += add
            total -= values[i][0]
        else:
            add = values[i][0] / values[i][1]
            retval += add
            total -= values[i][0]
            i += 1
    hour = floor(retval)
    minute = retval - hour
    minute = minute * 60
    minute = int(round(minute))

    #endtime = endtime.shift(minutes=+minute)
    endtime = endtime.shift(hours=+hour, minutes=+minute)

    return endtime.isoformat()
#print(open_time(200, 150, arrow.get("2017-01-01 00:00", 'YYYY-MM-DD HH:mm')))
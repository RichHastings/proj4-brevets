import acp_times
from acp_times import open_time
import arrow

testtime=arrow.get("2017-01-01 00:00:00", 'YYYY-MM-DD HH:mm:ss')

print(open_time(200, 150, arrow.get("2017-01-01 00:00", 'YYYY-MM-DD HH:mm')))

def test_open():
    assert acp_times.open_time(1000, 200, testtime) == acp_times.open_time(200, 201, testtime)
    test2=arrow.get("2017-01-01 15:28", 'YYYY-MM-DD HH:mm')
    assert acp_times.open_time(1000, 500, testtime) == test2.isoformat()
    test2=arrow.get("2017-01-02 01:57", 'YYYY-MM-DD HH:mm')
    assert acp_times.open_time(1000, 800, testtime) == test2.isoformat()
    test2 = arrow.get("2017-01-02 09:05", 'YYYY-MM-DD HH:mm')
    assert acp_times.open_time(1000, 1000, testtime) == test2.isoformat()


def test_close():
    assert acp_times.close_time(200, 200, testtime) == testtime.shift(hours=+13, minutes=+30).isoformat()
    print(acp_times.close_time(200, 200, testtime))
    print(acp_times.close_time(200, 201, testtime))
    assert acp_times.close_time(200, 200, testtime) == acp_times.close_time(201, 200, testtime)
    assert acp_times.close_time(200, 201, testtime) != acp_times.close_time(200, 200, testtime)
    test2 = arrow.get("2017-01-02 09:20", 'YYYY-MM-DD HH:mm')
    assert acp_times.close_time(1000, 500, testtime) ==test2.isoformat()
    test2 = arrow.get("2017-01-03 09:30", 'YYYY-MM-DD HH:mm')
    assert acp_times.close_time(1000, 800, testtime) ==test2.isoformat()
    test2 = arrow.get("2017-01-04 03:00", 'YYYY-MM-DD HH:mm')
    assert acp_times.close_time(1000, 1000, testtime) == test2.isoformat()

def test_edge():
    assert acp_times.open_time(200, 0, testtime) == testtime.isoformat()
    assert acp_times.open_time(201, 200, testtime) == acp_times.open_time(200, 200, testtime)
    assert acp_times.open_time(200, 201, testtime) == acp_times.open_time(200, 200, testtime)
    assert acp_times.close_time(200, 0, testtime) != testtime.isoformat()

    assert acp_times.close_time(200, 0, testtime) == testtime.shift(hours=+1).isoformat()
    assert acp_times.close_time(200, 15, testtime) == testtime.shift(hours=+1).isoformat()

def test_diff():
    assert acp_times.close_time(200, 200, testtime) != acp_times.open_time(200, 200, testtime)
    assert acp_times.close_time(1000, 0, testtime) != acp_times.open_time(1000, 0, testtime)
    assert acp_times.close_time(1000, 601, testtime) != acp_times.open_time(1000, 601, testtime)
    assert acp_times.close_time(1000, 1001, testtime) != acp_times.open_time(1000, 1001, testtime)

